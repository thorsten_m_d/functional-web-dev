// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

import socket from "./socket"

window.App =  {
  new_channel(subtopic, screen_name) {
    let channel = socket.channel("game:"+subtopic, {screen_name: screen_name});
    channel.on("said_hello", resp => console.log("Returned ", resp.message))
    channel.on("player_added", resp => console.log("Player Added", resp.message));

    return channel;
  },

  join(channel) {
    channel.join()
    .receive("ok", resp => { console.log("Joined successfully", resp) })
    .receive("error", resp => { console.log("Unable to join", resp) })
  },

  leave(channel) {
    channel.leave()
      .receive("ok", resp => { console.log("Left successfully", resp) } )
      .receive("error", resp => { console.log("Unable to leave", resp) } )
  },

    say_hello(channel, greeting) {
    channel.push("hello", {message: greeting})
  },

  new_game(channel) {
    channel.push("new_game")
    .receive("ok", resp => console.log("New Game!", resp))
    .receive("error", resp => console.log("Unable to start a new game", resp))
  },

  add_player(channel, player) {
    channel.push("add_player", player)
    .receive("error", console.log)
  }
}
