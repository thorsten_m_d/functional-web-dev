defmodule IslandsEngine.Island do
  alias IslandsEngine.Coordinate

  def start_link() do
    Agent.start_link(fn -> [] end)
  end

  def replace_coordinates(island, new_coordinates) when is_list(new_coordinates) do
    Agent.update(island, fn _state -> new_coordinates end)
  end

  def coordinates(island) do
    Agent.get(island, &(&1))
  end

  def forested?(island) do
    island
    |> coordinates
    |> Enum.all?(&Coordinate.hit?/1)
  end

  def to_string(island) do
    island_string =
      island
      |> coordinates
      |> Enum.map_join(", ", &Coordinate.to_string/1)

      "[" <> island_string <> "]"
  end
end
