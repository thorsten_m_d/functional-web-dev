defmodule IslandsEngine.Board do
  alias IslandsEngine.Coordinate

  def start_link do
    Agent.start_link(fn -> initialized_board() end)
  end

  def get_coordinate(board, key) when is_atom key do
    Agent.get(board, fn board -> board[key] end)
  end

  def guess_coordinate(board, key) do
    board
    |> get_coordinate(key)
    |> Coordinate.guess
  end

  def coordinate_hit?(board, key) do
    board
    |> get_coordinate(key)
    |> Coordinate.hit?
  end

  def set_coordinate_in_island(board, key, island) do
    board
    |> get_coordinate(key)
    |> Coordinate.set_in_island(island)
  end

  def coordinate_island(board, key) do
    board
    |> get_coordinate(key)
    |> Coordinate.island
  end

  def to_string(board) do
    "%{\n" <> string_body(board) <> "\n}"
  end

  defp string_body(board) do
    Enum.map_join(keys(), "\n", fn key ->
      coord = get_coordinate(board, key)
      "#{key} #{Coordinate.to_string(coord)}"
    end)
  end

  @letters ~W(a b c d e f g h i j)
  @numbers [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

  defp keys() do
    for letter <- @letters, number <- @numbers do
      String.to_atom("#{letter}#{number}")
    end
  end

  defp initialized_board() do
    for key <- keys(), into: %{} do
      {:ok, coord} = Coordinate.start_link
      {key, coord}
    end
  end
end
